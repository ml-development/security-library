<?php
/**
 * Created by IntelliJ IDEA.
 * User: matthewl
 * Date: 11/15/18
 * Time: 9:07 AM
 */

class Filters
{
    const SANITIZE_HTML = 1;
    const SANITIZE_ENCODED = 2;
    const SANITIZE_INT = 3;
    const SANITIZE_EMAIL = 4;
    const SANITIZE_QUOTES = 5;
    const SANITIZE_FLOAT = 6;
    const SANITIZE_SPECIAL_CHARS = 7;
    const SANITIZE_STRING = 8;
    const SANITIZE_URL = 9;
    const SANITIZE_UNSAFE = 10;
}

class Security
{
    /**
     * @see filter_var()
     * @param mixed $value
     * @param array $filters
     * @return mixed
     */
    public function filter_array(array $array, array $filters)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $key => $z) {
                    $z = $this->sanitize($z, $filters);
                    if ($z === FALSE) {
                        return $z;
                    }
                }
            } else {
                $value = $this->sanitize($value, $filters);
            }
        }
        return $value;
    }

    /**
     * @see filter_var()
     * @param mixed $value
     * @param array $filters
     * @return mixed
     */
    public function filter_input($value, array $filters)
    {
        if (is_array($value)) {
            foreach ($value as $key => $z) {
                $z = $this->sanitize($z, $filters);
                if ($z === FALSE) {
                    return $z;
                }
            }
        } else {
            $value = $this->sanitize($value, $filters);
        }
        return $value;
    }
    private function sanitize($value, array $filters)
    {
        foreach ($filters as $filter) {
            switch ($filter) {
                case Filters::SANITIZE_INT:
                    $value = $this->sanitize_int($value);
                    break;
                case Filters::SANITIZE_ENCODED:
                    $value = $this->sanitize_encoded($value);
                    break;
                case Filters::SANITIZE_EMAIL:
                    $value = $this->sanitize_email($value);
                    break;
                case Filters::SANITIZE_QUOTES:
                    $value = $this->sanitize_quotes($value);
                    break;
                case Filters::SANITIZE_FLOAT:
                    $value = $this->sanitize_float($value);
                    break;
                case Filters::SANITIZE_SPECIAL_CHARS:
                    $value = $this->sanitize_special_char($value);
                    break;
                case Filters::SANITIZE_STRING:
                    $value = $this->sanitize_string($value);
                    break;
                case Filters::SANITIZE_URL:
                    $value = $this->sanitize_url($value);
                    break;
                case Filters::SANITIZE_UNSAFE:
                    $value = $this->sanitize_unsafe($value);
                    break;
            }
            if ($value === FALSE) {
                return $value;
            }
        }
        return $value;
    }

    /**
     * @see filter_var()
     * @param int $value
     * @return mixed
     */
    public function sanitize_int(int $value)
    {
        return filter_var($value, FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * @see filter_var();
     * @param string $value
     * @return mixed
     */
    public function sanitize_encoded(string $value)
    {
        return filter_var($value, FILTER_SANITIZE_ENCODED);
    }

    /**
     * @see filter_var();
     * @param string $value
     * @return mixed
     */
    public function sanitize_email(string $value)
    {
        return filter_var($value, FILTER_SANITIZE_EMAIL);
    }

    /**
     * @see filter_var()
     * @param mixed $value
     * @return mixed
     */
    public function sanitize_quotes(string $value)
    {
        return filter_var($value, FILTER_SANITIZE_MAGIC_QUOTES);
    }

    /**
     * @see filter_var()
     * @param $value
     * @return mixed
     */
    public function sanitize_float($value)
    {
        return filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT);
    }

    /**
     * @see filter_var()
     * @param mixed $value
     * @return mixed
     */
    public function sanitize_special_char(string $value)
    {
        return filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS);
    }

    /**
     * @see filter_var()
     * @param string $value
     * @return mixed
     */
    public function sanitize_string(string $value)
    {
        return filter_var($value, FILTER_SANITIZE_STRING);
    }

    /**
     * @see filter_var()
     * @param string $value
     * @return mixed
     */
    public function sanitize_url( $value)
    {
        return filter_var($value, FILTER_SANITIZE_URL);
    }

    public function sanitize_unsafe($value) {
        return filter_var($value, FILTER_UNSAFE_RAW);
    }
}